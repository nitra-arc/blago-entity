<?php

namespace Nitra\BlagoEntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Nitra\BlagoEntityBundle\Repository\SettingsRepository")
 */
class Settings
{

    /**
     * @var integer
     *
     * @ORM\Column( type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column( type="array", nullable=true)
     */
    private $phones;

    /**
     * @var string
     * @ORM\Column( type="string", length=255)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column( type="string", length=255)
     */
    private $contactEmail;

    /**
     * @var string
     * @ORM\Column( type="string", length=255)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $analitics;

    /**
     * @var integer
     * @ORM\Column( type="integer" )
     */
    private $perPage;

    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set contactEmail
     * @param string $contactEmail
     * @return Contact
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string 
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Contact
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set analitics
     * @param string $analitics
     * @return Contact
     */
    public function setAnalitics($analitics)
    {
        $this->analitics = $analitics;

        return $this;
    }

    /**
     * Get analitics
     *
     * @return string 
     */
    public function getAnalitics()
    {
        return $this->analitics;
    }

    /**
     * Set count news
     *
     * @param string $countNews
     * @return Contact
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;

        return $this;
    }

    /**
     * Get count news
     * @return string 
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    public function __toString()
    {
        return $this->id;
    }


    /**
     * Set phones
     *
     * @param \Array $phones
     * @return Settings
     */
    public function setPhones(Array $phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return \Array 
     */
    public function getPhones()
    {
        return $this->phones;
    }
}
