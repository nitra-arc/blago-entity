<?php

namespace Nitra\BlagoEntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Nitra\BlagoEntityBundle\Repository\ArticleRepository")
 */
class Article
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="article", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="RESTRICT" )
     * 
     * @Assert\Type(type="Nitra\BlagoEntityBundle\Entity\Category")
     */
    private $category;

    /**
     * @Assert\NotBlank(message="Ошибка. Укажите название.")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=200, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $introtext;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column( type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column( type="integer")
     */
    private $sorting;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $showForm;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $onMain;

    /**
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Article
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $introtext
     * @return Article
     */
    public function setIntrotext($introtext)
    {
        $this->introtext = $introtext;

        return $this;
    }

    /**
     * @return string 
     */
    public function getIntrotext()
    {
        return $this->introtext;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $image
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param integer $sorting
     * @return Article
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * @return integer 
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param boolean $isActive
     * @return Article
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    /**
     * @param boolean $showForm
     * @return Article
     */
    public function setShowForm($showForm)
    {
        $this->showForm = $showForm;

        return $this;
    }

    /**
     * @return boolean 
     */
    public function getShowForm()
    {
        return $this->showForm;
    }
    
    /**
     * @param boolean $onMain
     * @return Article
     */
    public function setOnMain($onMain)
    {
        $this->onMain = $onMain;

        return $this;
    }

    /**
     * @return boolean 
     */
    public function getOnMain()
    {
        return $this->onMain;
    }

    /**
     * @param \Nitra\BlagoEntityBundle\Entity\Category $category
     * @return Article
     */
    public function setCategory(\Nitra\BlagoEntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return \Nitra\BlagoEntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
