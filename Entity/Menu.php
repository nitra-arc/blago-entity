<?php

namespace Nitra\BlagoEntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Nitra\BlagoEntityBundle\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="RESTRICT" )
     * 
     * @Assert\Type(type="Nitra\BlagoEntityBundle\Entity\Category")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Article", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="RESTRICT" )
     * 
     * @Assert\Type(type="Nitra\BlagoEntityBundle\Entity\Article")
     */
    private $article;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=200, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column( type="integer")
     */
    private $sorting;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;
    
    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;
    
     /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Menu
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return Menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param string $type
     * @return Menu
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param integer $sorting
     * @return Article
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * @return integer 
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param boolean $isActive
     * @return Article
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param \Nitra\BlagoEntityBundle\Entity\Category $category
     * @return Article
     */
    public function setCategory(\Nitra\BlagoEntityBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return \Nitra\BlagoEntityBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param \Nitra\BlagoEntityBundle\Entity\Category $category
     * @return Article
     */
    public function setArticle(\Nitra\BlagoEntityBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return \Nitra\BlagoEntityBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    public function __toString()
    {
        return (string) $this->name;
    }
    
    public function setParent(Menu $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

}
