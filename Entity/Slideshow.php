<?php

namespace Nitra\BlagoEntityBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Slideshow
 *
 * @ORM\Table
 * @ORM\Entity(repositoryClass="Nitra\BlagoEntityBundle\Repository\SlideshowRepository")
 */
class Slideshow
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Column( type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column( type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column( type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column( type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Ошибка. Пожалуйста загрузите изображение")
     */
    private $image;

    /**
     * @var string
     * @ORM\Column( type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     * @ORM\Column( type="integer", nullable=true)
     */
    private $sorting;

    /**
     * @ORM\Column( type="boolean")
     */
    private $isActive;

    /**
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return Slideshow
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $description
     * @return Slideshow
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $image
     * @return Slideshow
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set link
     *
     * @param string $image
     * @return Slideshow
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set sorting
     *
     * @param integer $sorting
     * @return Slideshow
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * Get sorting
     *
     * @return integer 
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Slideshow
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

}
