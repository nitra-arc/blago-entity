<?php

namespace Nitra\BlagoEntityBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository as NestedTreeRepository;
//use Doctrine\Common\Collections\Criteria;

/**
 * CategoryRepository
 */
class CategoryRepository extends NestedTreeRepository
{
    /** path for  breadcrumbs  */
    protected $path = array();
    
    /**
     * Получение категорий в древовидной структуре
     * @return type
     */
    public function getTreeNames()
    {
        return $this->getChildrenQueryBuilder()
                        ->add('orderBy', 'node.root ASC, node.lft ASC');
    }
    
    public function getTreeNamesWithoutSelf($category)
    {
        $qb = $this->getTreeNames();
        
//        if($category) {
//            $expr = $criteria->andWhere('id !=:id')->setParameter('id', $category->getId());
//            $qb->addCriteria($expr);
//        }
        
        return $qb;
    }
    /**
     * Категории для карусели на главной
     * @param array $sort
     * @return type
     */
    public function getCategoryOnMain(array $sort = null)
    {
        $categories = $this->findBy(array('onMain' => true), $sort);
        
        return $categories;
    }
    
    /**
     * Получаем дерево родиельских категрий
     * @param type $category
     * @return type collection category
     */
    public function getParentsCategory($category)
    {
        $this->path[] = $category;
        
        if ($category->getParent()) {
            return $this->getParentsCategory($category->getParent());
        }
        
        return array_reverse($this->path);
        
    }
    
    
}
