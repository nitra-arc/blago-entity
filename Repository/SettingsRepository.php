<?php

namespace Nitra\BlagoEntityBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * SettingsRepository
 */
class SettingsRepository extends EntityRepository
{
    
    public function getSettings()
    {
        // находим настройки
        return $this->findOneBy(array(), array('id' => 'asc'));
    }

}
